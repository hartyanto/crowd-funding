<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Admin 1',
            'email' => 'admin1@crowdfunding.com',
            'password' => bcrypt('password'),
            'role_id' => 2,
        ]);

        App\User::create([
            'name' => 'Admin 2',
            'email' => 'admin2@crowdfunding.com',
            'password' => bcrypt('password'),
            'role_id' => 1,
        ]);

        App\User::create([
            'name' => 'User 1',
            'email' => 'user1@crowdfunding.com',
            'password' => bcrypt('password'),
            'role_id' => 1,
        ]);

        App\User::create([
            'name' => 'User 2',
            'email' => 'user2@crowdfunding.com',
            'password' => bcrypt('password'),
            'role_id' => 1,
        ]);
    }
}
