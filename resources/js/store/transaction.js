export default {
    namespaced: true,
    state: {
        transactions: 0,
    },
    getters: {
        checkTransactions(state) {
            return state.transactions
        }
    },
    mutations: {
        ADD_TRANSACTION (state) {
            state.transactions++
        },
    },
    actions: {
        add_transaction({commit}) {
            commit('ADD_TRANSACTION')
        },
    }
}