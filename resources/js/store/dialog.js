export default {
    namespaced: true,
    state: {
        status: false,
        component: 'search',
    },
    mutations: {
        SET_STATUS(state, status) {
            state.status = status
        },
        SET_COMPONENT(state, component) {
            state.component = component
        },
    },
    actions: {
        setStatus({commit}, status) {
            commit('SET_STATUS', status)
        },
        setComponent({commit}, component) {
            commit('SET_COMPONENT', component)
            commit('SET_STATUS', true)
        },
    },
    getters: {
        status: state => state.status,
        component: state => state.component
    }
}