import Axios from "axios"

export default {
    namespaced: true,
    state: {
        user: {},
    },
    mutations: {
        SET(state, payload) {
            state.user = payload
        },
    },
    actions: {
        set({commit}, payload) {
            commit('SET', payload)
        },

        async checkToken({commit}, payload) {
            let config = {
                headers: {
                    'Authorization': 'Bearer ' + payload.token,
                },
            }

            await Axios.post('/api/auth/check-token', {}, config)
            .then((response) => {
                commit('SET', payload)
            }).catch((err) => {
                commit('SET', {})
            });
        }
    },
    getters: {
        user    : state => state.user,
        guest   : state => Object.keys(state.user).length === 0,
    }
}