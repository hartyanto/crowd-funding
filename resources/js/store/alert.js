export default {
    namespaced: true,
    state: {
        status  : false,
        color   : 'success',
        text    : ''
    },
    mutations: {
        SET_ALERT: (state, payload) => {
            state.status    = payload.status
            state.text      = payload.text
            state.color     = payload.color
        }
    },
    actions: {
        set_alert: ({commit}, payload) => {
            commit('SET_ALERT', payload)
        }
    },
    getters: {
        status  : state => state.status,
        color   : state => state.color,
        text    : state => state.text
    }
}