import BusEvent from './bus'

Echo.join('chat-channel')
    .listen('ChatStoredEvent', (e) => {
        let formData = {
            'subject': e.data.subject,
            'from': e.data.user.name,
            'from_id': e.data.user.id,
            'to': e.to
        }
        BusEvent.$emit('chat-sent', formData)
    });