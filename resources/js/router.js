import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            alias: '/home',
            name: 'home',
            component: () => import('./views/Home.vue')
        },
        {
            path: '/donation/:id',
            name: 'donation',
            component: () => import('./views/Donations.vue')
        },
        {
            path: '/campaigns',
            name: 'campaigns',
            component: () => import('./views/Campaigns.vue')
        },
        {
            path: '/campaign/:id',
            name: 'campaign',
            component: () => import('./views/Campaign.vue')
        },
        {
            path: '/new-campaign',
            name: 'newCampaign',
            component: () => import('./views/NewCampaign.vue')
        },
        {
            path: '/profile',
            name: 'profile',
            component: () => import('./views/Profile.vue')
        },
        {
            path: '/auth/social/:provider/callback',
            name: 'social',
            component: () => import('./views/Social.vue')
        },
        {
            path: '/admin',
            name: 'admin',
            component: () => import('./views/Admin.vue')
        },
        {
            path: '/chat-admin/:id',
            name: 'chatAdmin',
            component: () => import('./views/ChatAdmin.vue')
        },
        {
            path: '/ask-admin',
            name: 'askAdmin',
            component: () => import('./views/AskAdmin.vue')
        },
        {
            path: '*',
            redirect: '/'
        },
    ]
})

export default router