<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div style="width: 100%;
                padding: 2rem 4rem;
                background-color:  rgb(0, 146, 232);">
        <h2 style="color: white;">Permintaan kode OTP</h2>
    </div>
    <div style="padding-left: 2rem;
                width: 100%;">
        <p style="color: rgb(131, 143, 150);">Kepada Bapak/Ibu {{$name }} permintaan buat ulang kode OTP telah diterima. <br/>Berikut kode OTP untuk melanjutkan proses registrasi anda :</p>
        <div style="display: flex;
                    justify-content: center;
                    width: 100%;">
            <div style="width: 50%;
                        margin: 2rem 0;
                        background-color: rgb(187, 240, 247);
                        border-radius: 1rem;">
                <div>
                    <div style="padding: 0.5rem;
                                text-align: center;
                                background-color: rgb(0, 146, 232);
                                border-radius: 1rem 1rem 0 0;"
                                >
                        <h4 style="color: white;">KODE OTP</h4>
                    </div>
                    <div style="padding: 0.5rem;
                                text-align: center;">
                        <h4>{{ $otp }}</h4>
                    </div>
                </div>
            </div>
        </div>
        <p style="color: rgb(131, 143, 150);">Salam Hangat,<br/> admin CrowdFunding</p>
    </div>
    <div style="width: 100%;
                padding: 1rem 4rem;
                background-color:  #AEAEAE">
        <p style="color: white;">PT. CrowdFunding <br/>Alamat dan kontak perusahaan</p>
    </div>
</body>
</html>