@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Route 1</div>
                <div class="card-body">
                    Berhasil masuk sebagai user/admin setelah verifikasi email
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
