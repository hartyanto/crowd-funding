<?php

use Illuminate\Support\Facades\Route;

Route::prefix('auth')->namespace('Auth')->middleware('api')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('verification', 'VerificationController');
    Route::post('regenerate-otp', 'RegenerateOtpController');
    Route::post('update-password', 'UpdatePasswordController');
    Route::post('login', 'LoginController');
    Route::post('check-token', 'CheckTokenController')->middleware('auth:api');
    Route::post('logout', 'LogoutController')->middleware('auth:api');

    Route::get('/social/{provider}', 'SocialiteController@redirectToProvider');
    Route::get('/social/{provider}/callback', 'SocialiteController@handleProviderCallback');
});

Route::middleware('auth:api', 'email.verified')->prefix('profile')->namespace('User')->group(function () {
    Route::get('get-profile', 'UserController@show');
    Route::post('update-profile', 'UserController@update');
});

Route::group([
    'middleware' => ['api', 'auth:api', 'email.verified'],
], function () {
    Route::get('/chat-admin', 'ChatController@chatAdmin');
    Route::get('/ask-admin/{id}', 'ChatController@index');
    Route::post('/ask-admin/store', 'ChatController@store');
});

// test middleware isAdmin
Route::get('/admin', function () {
    return response()->json([
        'Response_code' => '00',
        'Response_message' => 'Berhasil masuk sebagai admin.',
    ]);
})->middleware('auth:api', 'email.verified', 'admin');


Route::group([
    'middleware'    => 'api',
    'prefix'        => 'campaign',
], function () {
    Route::get('/', 'CampaignController@index');
    Route::get('/{id}', 'CampaignController@show');
    Route::get('random/{count}', 'CampaignController@random');
    Route::post('store', 'CampaignController@store')->middleware('auth:api');
    Route::get('search/{keyword}', 'CampaignController@search');
});

Route::group([
    'middleware' => 'api',
    'prefix'    => 'blog',
], function () {
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});

Route::post('payment/store', 'PaymentController@store');
