(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Donations.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Donations.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      e1: 1,
      amounts: [10000, 25000, 50000, 100000],
      amount: 0,
      amountRules: [function (v) {
        return v && v >= 10000 || "Min. Rp 10.000";
      }],
      methods: [{
        method: "permata",
        text: "Bank Permata Virtual Account"
      }],
      method: "",
      methodText: "",
      valid: false,
      order: {},
      loading: false
    };
  },
  methods: {
    toStepTwo: function toStepTwo(value) {
      this.amount = value;
      this.e1 = 2;
    },
    requestOrder: function requestOrder(method, text) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var formData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this.loading = true, _this.method = method;
                _this.methodText = text;

                if (!_this.$refs.form.validate()) {
                  _context.next = 6;
                  break;
                }

                formData = {
                  method: _this.method,
                  amount: _this.amount
                };
                _context.next = 6;
                return axios.post("/api/payment/store", formData).then(function (response) {
                  var data = response.data.data;
                  _this.order = data;
                  _this.e1 = 3;
                  _this.loading = false;
                })["catch"](function (err) {
                  _this.loading = false;
                });

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  },
  mounted: function mounted() {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Donations.vue?vue&type=template&id=bcbef6dc&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Donations.vue?vue&type=template&id=bcbef6dc& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    [
      _c(
        "v-card",
        [
          _c(
            "v-stepper",
            {
              attrs: { "alt-labels": "" },
              model: {
                value: _vm.e1,
                callback: function($$v) {
                  _vm.e1 = $$v
                },
                expression: "e1"
              }
            },
            [
              _c(
                "v-stepper-header",
                [
                  _c(
                    "v-stepper-step",
                    { attrs: { step: "1", complete: _vm.e1 > 1 } },
                    [_vm._v("\n          Pilih Nominal Donasi\n        ")]
                  ),
                  _vm._v(" "),
                  _c("v-divider"),
                  _vm._v(" "),
                  _c(
                    "v-stepper-step",
                    { attrs: { step: "2", complete: _vm.e1 > 2 } },
                    [_vm._v("\n          Pilih Metode Pembayaran\n        ")]
                  ),
                  _vm._v(" "),
                  _c("v-divider"),
                  _vm._v(" "),
                  _c("v-stepper-step", { attrs: { step: "3" } }, [
                    _vm._v("Pembayaran ")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-stepper-items",
                [
                  _c(
                    "v-stepper-content",
                    { attrs: { step: "1" } },
                    [
                      _c(
                        "v-card",
                        { staticClass: "pa-4" },
                        [
                          _vm._l(_vm.amounts, function(amount) {
                            return _c(
                              "v-card",
                              {
                                key: amount,
                                staticClass: "my-4",
                                on: {
                                  click: function($event) {
                                    return _vm.toStepTwo(amount)
                                  }
                                }
                              },
                              [
                                _c("v-card-title", [
                                  _vm._v(
                                    "Rp " +
                                      _vm._s(amount.toLocaleString("id-ID"))
                                  )
                                ])
                              ],
                              1
                            )
                          }),
                          _vm._v(" "),
                          _c(
                            "v-card",
                            [
                              _c("v-card-title", [_vm._v("Lainnya")]),
                              _vm._v(" "),
                              _c(
                                "v-form",
                                {
                                  staticClass: "px-4",
                                  model: {
                                    value: _vm.valid,
                                    callback: function($$v) {
                                      _vm.valid = $$v
                                    },
                                    expression: "valid"
                                  }
                                },
                                [
                                  _c("v-text-field", {
                                    staticClass: "h3",
                                    attrs: {
                                      outlined: "",
                                      rules: _vm.amountRules,
                                      type: "number",
                                      prefix: "Rp"
                                    },
                                    model: {
                                      value: _vm.amount,
                                      callback: function($$v) {
                                        _vm.amount = $$v
                                      },
                                      expression: "amount"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "v-layout",
                                    [
                                      _c(
                                        "v-btn",
                                        {
                                          staticClass: "mx-auto mb-4",
                                          attrs: {
                                            disabled: !_vm.valid,
                                            color: "primary"
                                          },
                                          on: {
                                            click: function($event) {
                                              _vm.e1 = 2
                                            }
                                          }
                                        },
                                        [_vm._v("Next")]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        2
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-stepper-content",
                    { attrs: { step: "2" } },
                    [
                      _c(
                        "v-card",
                        { attrs: { loading: _vm.loading } },
                        [
                          _c(
                            "template",
                            { slot: "progress" },
                            [
                              _c("v-progress-linear", {
                                attrs: {
                                  color: "deep-purple",
                                  height: "10",
                                  indeterminate: ""
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("v-card-title", [_vm._v("Donasi")]),
                          _vm._v(" "),
                          _c(
                            "v-form",
                            {
                              ref: "form",
                              staticClass: "px-4",
                              attrs: { "lazy-validation": "" },
                              model: {
                                value: _vm.valid,
                                callback: function($$v) {
                                  _vm.valid = $$v
                                },
                                expression: "valid"
                              }
                            },
                            [
                              _c("v-text-field", {
                                staticClass: "h3",
                                attrs: {
                                  outlined: "",
                                  rules: _vm.amountRules,
                                  type: "number",
                                  prefix: "Rp"
                                },
                                model: {
                                  value: _vm.amount,
                                  callback: function($$v) {
                                    _vm.amount = $$v
                                  },
                                  expression: "amount"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("v-card-title", [_vm._v("Pembayaran")]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "pa-4" },
                            _vm._l(_vm.methods, function(m) {
                              return _c(
                                "v-card",
                                {
                                  key: m.method,
                                  on: {
                                    click: function($event) {
                                      return _vm.requestOrder(m.method, m.text)
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "v-card-title",
                                    [
                                      _c(
                                        "v-avatar",
                                        {
                                          staticClass: "mr-4",
                                          attrs: { tile: "", width: "50" }
                                        },
                                        [
                                          _c("img", {
                                            attrs: {
                                              src:
                                                "/photos/other/" +
                                                m.method +
                                                ".png"
                                            }
                                          })
                                        ]
                                      ),
                                      _vm._v(
                                        "\n                  " + _vm._s(m.text)
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            }),
                            1
                          )
                        ],
                        2
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-stepper-content",
                    { attrs: { step: "3" } },
                    [
                      _c(
                        "v-card",
                        [
                          _c("v-card-title", [
                            _vm._v("Silahkan melakukan pembayaran")
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "pa-4" },
                            [
                              _c(
                                "v-card",
                                [
                                  _c(
                                    "div",
                                    { staticClass: "d-flex justify-center" },
                                    [
                                      _c("img", {
                                        attrs: {
                                          width: "300",
                                          src:
                                            "/photos/other/" +
                                            _vm.method +
                                            ".png"
                                        }
                                      })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-list-item",
                                    [
                                      _c(
                                        "v-list-item-content",
                                        { staticClass: "py-4" },
                                        [
                                          _c(
                                            "v-list-item-title",
                                            {
                                              staticClass:
                                                "headline text-center mt-4 mb-1"
                                            },
                                            [
                                              _vm._v(
                                                "\n                      " +
                                                  _vm._s(_vm.methodText) +
                                                  "\n                    "
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "h3 text-center mt-4"
                                            },
                                            [
                                              _vm._v(
                                                "\n                      " +
                                                  _vm._s(
                                                    _vm.order.permata_va_number
                                                  ) +
                                                  "\n                    "
                                              )
                                            ]
                                          ),
                                          _vm._v(" "),
                                          _vm.order.order_id
                                            ? _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "h3 text-center mt-4"
                                                },
                                                [
                                                  _vm._v(
                                                    "\n                      Rp " +
                                                      _vm._s(
                                                        _vm.order.gross_amount.toLocaleString(
                                                          "id-ID"
                                                        )
                                                      ) +
                                                      "\n                    "
                                                  )
                                                ]
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "h5 text-center mt-4"
                                            },
                                            [
                                              _vm._v(
                                                "\n                      Order Id : " +
                                                  _vm._s(_vm.order.order_id) +
                                                  "\n                    "
                                              )
                                            ]
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/views/Donations.vue":
/*!******************************************!*\
  !*** ./resources/js/views/Donations.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Donations_vue_vue_type_template_id_bcbef6dc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Donations.vue?vue&type=template&id=bcbef6dc& */ "./resources/js/views/Donations.vue?vue&type=template&id=bcbef6dc&");
/* harmony import */ var _Donations_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Donations.vue?vue&type=script&lang=js& */ "./resources/js/views/Donations.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Donations_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Donations_vue_vue_type_template_id_bcbef6dc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Donations_vue_vue_type_template_id_bcbef6dc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Donations.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Donations.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/views/Donations.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Donations_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Donations.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Donations.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Donations_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Donations.vue?vue&type=template&id=bcbef6dc&":
/*!*************************************************************************!*\
  !*** ./resources/js/views/Donations.vue?vue&type=template&id=bcbef6dc& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Donations_vue_vue_type_template_id_bcbef6dc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Donations.vue?vue&type=template&id=bcbef6dc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Donations.vue?vue&type=template&id=bcbef6dc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Donations_vue_vue_type_template_id_bcbef6dc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Donations_vue_vue_type_template_id_bcbef6dc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);