<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EmailVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->email_verified_at == !null) {
            return $next($request);
        }
        abort(403, 'Lakukan verifikasi email terlebih dahulu. Cek email kamu, link verifikasi sudah dikirimkan ke email kamu.');
    }
}
