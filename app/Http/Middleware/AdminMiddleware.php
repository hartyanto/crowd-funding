<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if ($user->isAdmin()) {
            return $next($request);
        }

        return response()->json([
            'Response_message' => 'Unauthorized. Halaman ini hanya bisa diakses oleh admin.',
        ], 403);
    }
}
