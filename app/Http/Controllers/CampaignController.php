<?php

namespace App\Http\Controllers;

use App\Http\Resources\CampaignResource;
use App\Models\Campaign;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function index()
    {
        $campaigns = Campaign::paginate(4);

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Data campaign berhasil ditampilkan!',
            'data'              => compact('campaigns'),
        ]);
    }

    public function show($id)
    {
        $campaign = Campaign::where('id', $id)->with('author')->first();
        $campaign = new CampaignResource($campaign);

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Data campaign berhasil ditampilkan!',
            'data'              => compact('campaign'),
        ], 200);
    }

    public function random($count)
    {
        $campaigns =  Campaign::select('*')
            ->inRandomOrder()
            ->limit($count)
            ->get();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Data campaigns berhasil ditampilkan.',
            'data' => compact('campaigns')
        ]);
    }

    public function store()
    {
        request()->validate([
            'title' => 'required',
            'address' => 'required',
            'description' => 'required',
            'image' => ['image', 'mimes:jpg,jpeg,png'],
            'required' => 'required',
        ]);

        $campaign = auth()->user()->campaigns()->create([
            'title' => request('title'),
            'address' => request('address'),
            'description' => request('description'),
            'required' => request('required')
        ]);

        if (request()->file('image')) {
            $image = request()->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $campaign->id . '.' . $image_extension;
            $image_folder = '/photos/campaigns/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $campaign->update([
                    'image' => $image_location,
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_message' => 'Photo campaign gagal diupload!',
                    'data' => compact('campaign'),
                ], 200);
            }
        }

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Data campaign berhasil ditambahkan!',
            'data' => compact('campaign')
        ], 200);
    }

    public function search($keyword)
    {
        $campaigns = Campaign::where('title', 'LIKE', '%' . $keyword . '%')->get();

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Data campaigns berhasil ditampilkan!',
            'data'              => compact('campaigns')
        ], 200);
    }
}
