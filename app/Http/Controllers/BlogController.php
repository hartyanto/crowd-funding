<?php

namespace App\Http\Controllers;

use App\Models\Blog;

class BlogController extends Controller
{
    public function random($count)
    {
        $blogs = Blog::select('*')
            ->inRandomOrder()
            ->limit($count)
            ->get();

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Blogs berhasil ditampilkan!',
            'data'              => compact('blogs')
        ]);
    }

    public function store()
    {
        request()->validate([
            'title' => 'required',
            'body'  => 'required',
            'image' => ['image', 'mimes:jpg,jpeg,png']
        ]);

        $blog = Blog::create([
            'title' => request('title'),
            'body'  => request('body')
        ]);

        if (request()->file('image')) {
            $image = request()->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $blog->id . '.' . $image_extension;
            $image_folder = '/photos/blogs/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);

                $blog->update([
                    'image' => $image_location,
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code'     => '01',
                    'response_message'  => 'Gambar gagal diupload!',
                    'data'              => compact('blog')
                ]);
            }
        }

        return response()->json([
            'response_code'     => '00',
            'response_message'  => 'Blog baru berhasil ditambahkan!',
            'data'              => compact('blog')
        ]);
    }
}
