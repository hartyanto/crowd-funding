<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function show()
    {
        $user = Auth::user();
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profil user berhasil ditampilkan.',
            'data' => compact('user')
        ]);
    }

    public function update()
    {
        if (request('photo_profile')) {
            request()->validate([
                'name' => ['string', 'required', 'min:3'],
                'photo_profile' => ['image', 'mimes:jpg,jpeg,png,svg', 'max:2048'],
            ]);
        } else {
            request()->validate([
                'name' => ['string', 'required', 'min:3'],
            ]);
        }

        $user = Auth::user();

        $photoProfile = request()->file('photo_profile');

        if ($photoProfile && $user->photo) {
            Storage::delete($user->photo);
            $photoProfileURL = $photoProfile->storeAs("images/user/photo_profiles", "{$user->id}.{$photoProfile->extension()}");
        } else if ($photoProfile && !$user->photo) {
            $photoProfileURL = $photoProfile->storeAs("images/user/photo_profiles", "{$user->id}.{$photoProfile->extension()}");
        } else if (!$photoProfile && $user->photo) {
            $photoProfileURL = $user->photo;
        } else {
            $photoProfileURL = null;
        }

        $user->update([
            'name' => request('name'),
            'photo' => $photoProfileURL,
        ]);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profil user berhasil diperbaharui.',
            'data' => compact('user')
        ]);
    }
}
