<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function store()
    {
        $transaction = Transaction::create([
            'amount' => request('amount'),
            'method' => request('method'),
        ]);

        $transaction->order_id = $transaction->id . "-" . \Str::random(5);

        $transaction->save();

        $response_midtrans = $this->midtrans_store($transaction);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Transaction Success',
            'data' => $response_midtrans
        ]);
    }

    protected function midtrans_store(Transaction $transaction)
    {
        $server_key = base64_encode(config('app.midtrans.server_key') . ':');
        $base_uri = config('app.midtrans.base_uri');
        $client = new Client([
            'base_uri' => $base_uri,
        ]);

        $headers = [
            'Accept'        => 'application/json',
            'Authorization' => 'Basic ' . $server_key,
            'Content-Type'  => 'application/json',
        ];

        switch ($transaction->method) {
            case 'permata':
                $body = [
                    "payment_type" => "bank_transfer",
                    "bank_transfer" => [
                        "bank" => "permata",
                        "permata" => [
                            "recipient_name" => "SUDARSONO"
                        ]
                    ],
                    "transaction_details" => [
                        "order_id" => $transaction->order_id,
                        "gross_amount" => $transaction->amount,
                    ]
                ];
                break;

            default:
                $body = [];
                break;
        }

        $res = $client->post('/v2/charge', [
            'headers' => $headers,
            'body' => json_encode($body),
        ]);

        return json_decode($res->getBody());
    }
}
