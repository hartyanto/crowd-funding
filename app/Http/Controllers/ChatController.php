<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use Illuminate\Http\Request;
use App\Events\ChatStoredEvent;
use App\Http\Resources\ChatAdminResource;
use App\Http\Resources\ChatResource;

class ChatController extends Controller
{
    public function index($from_id)
    {
        $id = auth()->id();

        if (auth()->user()->isAdmin()) {
            $chats = Chat::where('from', $from_id)->orWhere('to', $from_id)->get();
        } else {
            $chats = Chat::where('from', $id)->orWhere('to', $id)->get();
        }

        $chats = ChatResource::collection($chats);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Chats berhasil ditampilkan',
            'data' => compact('chats'),
        ]);
    }

    public function store()
    {
        request()->validate([
            'subject' => 'required',
        ]);

        $admin_uuid = User::where('email', 'admin@crowdfunding.pragaprabowo.site')->first()->id;

        if (auth()->user()->isAdmin()) {
            $id = User::where('name', request('to'))->first()->id;

            $chat = auth()->user()->chats()->create([
                'subject' => request('subject'),
                'to' => $id,
            ]);
        } else {
            $chat = auth()->user()->chats()->create([
                'subject' => request('subject'),
                'to' => $admin_uuid
            ]);
        }

        broadcast(new ChatStoredEvent($chat))->toOthers();

        return response()->json([
            'response_code' => '00',
            'response_message'  => 'Message sent successfully',
        ]);
    }

    public function chatAdmin()
    {
        $chats = Chat::select('from', \DB::raw('COUNT(subject) as amount'))->groupBy('from')->get();
        $chats = ChatAdminResource::collection($chats);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Chats berhasil ditampilkan',
            'data' => compact('chats'),
        ]);
    }
}
