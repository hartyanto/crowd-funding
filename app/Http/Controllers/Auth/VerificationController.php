<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Auth\Otp_code;
use Carbon\Carbon;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $otp = Otp_code::where('otp_code', request('otp'))->first();

        if (!$otp) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Kode OTP tidak ditemukan, silahkan cek kembali.'
            ]);
        }

        $time_now = Carbon::now();
        if ($otp->expired_at < $time_now) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Kode OTP sudah kadaluwarsa, silahkan buat ulang kode OTP.'
            ]);
        }

        $otp->user()->update([
            'email_verified_at' => Carbon::now(),
        ]);

        $otp->delete();
        $user = $otp->user;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Email berhasil diverifikasi.',
            'data' => compact('user'),
        ]);
    }
}
