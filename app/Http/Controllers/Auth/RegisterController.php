<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\UserRegisteredEvent;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'name' => ['string', 'required'],
            'email' => ['email', 'required', 'unique:users,email'],
        ]);

        User::create([
            'name' => request('name'),
            'email' => request('email'),
        ]);

        $user = User::where('email', request('email'))->first();

        event(new UserRegisteredEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Silahkan cek email',
            'data' => compact('user')
        ]);
    }
}
