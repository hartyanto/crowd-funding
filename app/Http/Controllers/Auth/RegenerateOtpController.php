<?php

namespace App\Http\Controllers\Auth;

use App\Events\OtpRequestEvent;
use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'email' => ['email', 'required']
        ]);

        $user = User::where('email', request('email'))->first();
        if (!$user) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email tidak ditemukan, silahkan daftar terlebih dahulu.'
            ]);
        }

        $time_now = Carbon::now();
        if ($user->otp_code->expired_at > $time_now) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Kode OTP anda belum kadaluwarsa, silahkan cek email.'
            ]);
        }

        $user->generate_otp_code();

        $user = User::where('email', request('email'))->first();
        event(new OtpRequestEvent($user));

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Kode OTP sudah dibuat ulang, silahkan cek email.',
            'data' => compact('user'),
        ]);
    }
}
