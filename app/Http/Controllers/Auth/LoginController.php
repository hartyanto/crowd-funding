<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'email' => ['email', 'required'],
            'password' => ['required']
        ]);

        if (!$token = auth()->attempt($request->only('email', 'password'))) {
            return response()->json(['error' => 'Email atau password tidak ditemukan'], 401);
        }

        $user = Auth::user();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Anda berhasil login',
            'data' => compact('token', 'user'),
        ]);
    }
}
