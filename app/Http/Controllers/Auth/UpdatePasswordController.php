<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'email' => ['email', 'required'],
            'password' => ['required', 'min:6', 'confirmed'],
        ]);

        $user = User::where('email', request('email'))->first();

        if (!$user) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email anda tidak ditemukan pastikan anda sudah mendaftar.'
            ]);
        }

        if ($user->password) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Unauthorized'
            ]);
        }

        if ($user->email_verified_at == null) {
            return response()->json([
                'response_code' => '01',
                'response_message' => 'Email anda belum diverifikasi, silahkan cek email.'
            ]);
        }

        $user->update([
            'password' => bcrypt(request('password'))
        ]);

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Password berhasil diperbaharui.',
            'data' => compact('user'),
        ]);
    }
}
