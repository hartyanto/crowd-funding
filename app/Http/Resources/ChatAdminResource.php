<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ChatAdminResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'amount'    => $this->amount,
            'from_id'   => $this->from,
            'from_name' => $this->user->name,
            'new_chat'  => false,
        ];
    }
}
