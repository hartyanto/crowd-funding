<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address' => $this->address,
            'image' => $this->image,
            'title' => $this->title,
            'required' => $this->required,
            'collected' => $this->collected,
            'description' => $this->description,
            'author' => $this->author->name,
            'author_email' => $this->author->email,
            'author_photo' => $this->author->photo,
        ];
    }
}
