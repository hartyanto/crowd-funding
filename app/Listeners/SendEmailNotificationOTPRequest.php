<?php

namespace App\Listeners;

use App\Events\OtpRequestEvent;
use App\Mail\OTPCodeRequest;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNotificationOTPRequest implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpRequestEvent  $event
     * @return void
     */
    public function handle(OtpRequestEvent $event)
    {
        Mail::to($event->user)->send(new OTPCodeRequest($event->user));
    }
}
