<?php

namespace App\Models;

use App\Traits\UsesUuid;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    use UsesUuid;

    protected $guarded = [];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_uuid');
    }
}
