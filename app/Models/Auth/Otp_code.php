<?php

namespace App\Models\Auth;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Otp_code extends Model
{
    protected $fillable = ['otp_code', 'expired_at', 'user_uuid'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_uuid');
    }
}
