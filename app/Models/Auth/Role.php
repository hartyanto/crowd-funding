<?php

namespace App\Models\Auth;

use App\User;
use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use UsesUuid;

    protected $fillable = ['name', 'id'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
