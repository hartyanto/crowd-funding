<?php

namespace App;

use App\Models\Auth\Role;
use App\Models\Auth\Otp_code;
use App\Models\Campaign;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements
    JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'photo', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected function get_user_role_id()
    {
        $role = Role::where('name', 'user')->first();
        return $role->id;
    }

    /**
     * The "booting" function of model
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (!$model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }

            $model->role_id = $model->get_user_role_id();
        });

        static::created(function ($model) {
            if (!$model->email_verified_at) $model->generate_otp_code();
        });
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function otp_code()
    {
        return $this->hasOne(Otp_code::class, 'user_uuid');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function isAdmin()
    {
        if ($this->role_id === $this->get_user_role_id()) {
            return false;
        }

        return true;
    }

    public function generate_otp_code()
    {
        do {
            $random = mt_rand(100000, 999999);
            $check = Otp_code::where('otp_code', $random)->first();
        } while ($check);

        $now = Carbon::now();

        Otp_code::updateOrCreate(
            ['user_uuid' => $this->id],
            [
                'otp_code' => $random,
                'expired_at' => $now->addMinutes(20)
            ]
        );
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class, 'user_uuid');
    }

    public function chats()
    {
        return $this->hasMany(Chat::class, 'from');
    }
}
